package space.brightone.vibercleaner

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.Toast
import com.tbruyelle.rxpermissions2.RxPermissions
import java.io.File
import java.util.jar.Manifest
import android.os.Process

class MainActivity : AppCompatActivity() {

    val TAG: String = "ViberCleaner"

    private fun deleteViberImages() {
        val result = arrayOf("Images", "Videos")
            .map { File(Environment.getExternalStorageDirectory(), "viber/media/Viber $it") }
            .filter { it.exists() && it.isDirectory }
            .map { dir -> dir.listFiles().map(File::delete).any { !it } }
            .any { !it }
        val message = if (result) R.string.delete_success else R.string.delete_error
        Toast.makeText(applicationContext, getString(message), Toast.LENGTH_SHORT).show()
    }

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rxPermissions = RxPermissions(this)
        rxPermissions.request(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
            .subscribe {
                if (it) {
                    findViewById<Button>(R.id.delete_button).setOnClickListener {
                        deleteViberImages()
                    }
                } else {
                    Toast.makeText(applicationContext, "Can't work without permission", Toast.LENGTH_SHORT).show()
                    Process.killProcess(Process.myPid())
                }
            }
    }

}
